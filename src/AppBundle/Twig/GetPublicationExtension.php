<?php

namespace AppBundle\Twig;

use Symfony\Bridge\Doctrine\RegistryInterface;

class GetPublicationExtension extends \Twig_Extension {

    protected $doctrine;

    public function __construct(RegistryInterface $doctrine) {
        $this->doctrine = $doctrine;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('get_publication', array($this, 'getPublicationFilter'))
        );
    }
    
    //El $id que le paso como parametro, puede ser cualquier nombre, que yo quiera...
    public function getPublicationFilter($id) {
        $publication_repo = $this->doctrine->getRepository("BackendBundle:Publication");
        $publication = $publication_repo->findOneBy(array(
            "id" => $id
        ));
        
        if(!empty($publication) && is_object($publication)){
            $result = $publication;
        }else{
           $result = false;
        }
        
        return $result;
    }
    
    public function getName() {
        return 'get_publication_extension';
    }

}
