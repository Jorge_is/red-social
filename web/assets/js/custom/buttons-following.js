function followButtons(){
    //con el 'unbind' evito que lancen varias veces el evento, cuando le de click al boton.
    $('.btn-follow').unbind("click").click(function(){
        $(this).addClass('hidden');
        $(this).parent().find('.btn-unfollow').removeClass('hidden');
        
            $.ajax({
               url: URL+'/follow',
               type: 'POST',
               data: {followed_ajax: $(this).attr("data-followed")},
               success: function(response){
                   console.log(response);
               }
            });
    });
    
    $('.btn-unfollow').unbind("click").click(function(){
        $(this).addClass('hidden');
        $(this).parent().find('.btn-follow').removeClass('hidden');
        
            $.ajax({
               url: URL+'/unfollow',
               type: 'POST',
               data: {followed_ajax: $(this).attr("data-followed")},
               success: function(response){
                   console.log(response);
               }
            });
    });
}