/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    var ias = jQuery.ias({
        container: '.box-users',
        item: '.user-item',
        pagination: '.pagination',
        next: '.pagination .next_link',
        triggerPageThreshold: 5
    });
    
    ias.extension(new IASTriggerExtension({
        text: 'Ver más personas',
        offset: 3
    }));
    
    ias.extension(new IASSpinnerExtension({
        src: URL+'/../assets/images/ajax-loader.gif'
    }));
    
    ias.extension(new IASNoneLeftExtension({
        text: 'No hay más personas'
    }));
    
    //capturo el ready cuando se cargan los items del listado
    ias.on('ready', function(event){
       followButtons() ;
    });
    
    //capturo el render cuando se renderiza otra pagina del listado
    ias.on('rendered', function(event){
       followButtons() ;
    });
    
});

