/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $(".form-nick").blur(function () {
        var nick = this.value;

        $.ajax({
            url: URL+'/nick-test',
            data: {nick: nick},
            type: 'POST',
            success: function (response) {
                if (response === "used") {
                    $(".form-nick").css("border", "1px solid red");
                } else {
                    $(".form-nick").css("border", "1px solid green");
                }
            }

        });
    });
});
